
class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    set name(value){
        this._name = value;
    }
    set age(value){
        this._age = value;
    }
    set salary(value){
        this._salary = value;
    }

    get name() {
        return this._name
    }
    get age() {
        return this._age
    }

    get salary() {
        return this._salary
    }

}

const employee = new Employee('Demian', 36, 10000);

class Programmer extends Employee {
    constructor(name, age, salary,lang) {
        super(name, age, salary);
        this._lang = lang;
    }
    set lang( value ) {
        this._lang = value
    }
    get lang() {
        return this._lang
    }

    set salary(value){
        this._salary = value;
    }
    get salary() {
        return this._salary * 3
    }
}

const programmer = new Programmer('Igor', 30, 9000, ['js', 'C++', 'php']);
const programmer2 = new Programmer('Mark', 36, 16000, ['js', 'Pascal', 'Delphi']);
const programmer3= new Programmer('Lena', 33, 11000, ['js', 'php']);

console.log(programmer);
console.log(programmer2);
console.log(programmer3);